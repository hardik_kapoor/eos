const express = require('express')
const router = express.Router()

const { strapiGraphql } = require('../../../modules/strapi-grapql-query')

/* eslint-disable */
router.get('/', async (req, res, next) => {
  const query = req.query.q

  try {
    const data = await strapiGraphql(query)

    return res.send(data)
  } catch (error) {
    console.log('error: ', error);
  }
})

module.exports = router
