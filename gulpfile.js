/* ==========================================================================
   Gulp - Builder manager for the Front-end
   ========================================================================== */

/* Configuration variables
   ========================================================================== */

/* Set dependencies required for the tasks */
const gulp = require('gulp'),
  concat = require('gulp-concat'),
  filter = require('gulp-filter'),
  rename = require('gulp-rename'),
  gulpMain = require('./modules/gulp-main.js'),
  clean = require('gulp-clean'),
  minify = require('gulp-minify'),
  cleanCSS = require('gulp-clean-css'),
  { series, parallel } = require('gulp')

/* Set the folders to read and inject vendor files */
const destination = 'vendors/build/'
const origin = 'vendors/'

/* Set the filters */
const jsFilter = filter('**/*.js'),
  cssFilter = filter('**/*.css'),
  /* we need to filter out MD fonts as it will have its own filter */
  fontFilter = filter(['**/*.{otf,eot,svg,ttf,woff,woff2}', '!**/MaterialIcons-Regular.{otf,eot,svg,ttf,woff,woff2}'], {restore: true})
  mdIconsFilter = filter('**/MaterialIcons-Regular.{otf,eot,svg,ttf,woff,woff2}')

/* Building tasks
   ========================================================================== */

/* Clean the built folder to start a fresh building */
const cleanMain = () => {
  return gulp.src(destination, { read: false, allowEmpty: true } )
    .pipe(clean())
}

/* Extract all css files declared in the mainfiles object */
const extractCss = () => {
  return gulp.src(gulpMain(origin), { allowEmpty: true })
    .pipe(cssFilter)
    .pipe(concat('vendors.css'))
    .pipe(cleanCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(`${destination}css`))
}

/* Extract all js files declared in the mainfiles object */
const extractJs = () => {
  return gulp.src(gulpMain(origin), { allowEmpty: true })
    .pipe(jsFilter)
    .pipe(concat('vendors.js'))
    .pipe(minify({ext:{
        min:'.min.js'
      }}))
    .pipe(gulp.dest(`${destination}js`))
}

/* Extract all font files declared in the mainfiles object */
const extractFonts = () => {
  return gulp.src(gulpMain(origin), { allowEmpty: true })
    .pipe(fontFilter)
    .pipe(gulp.dest(destination + 'fonts')) // move all fonts, except for MD icons to the /fonts folder as per fontawesome and eos-icons default configuration
    .pipe(fontFilter.restore)
    .pipe(mdIconsFilter)
    .pipe(gulp.dest(`${destination}css`)) // Material icons .css file is configured to have the css and fonts in the same folder
}

/* Add map files to the build so that the web inspector doesn't throw and error */
const moveBootstrapMap = () => {
  return gulp.src('vendors/node_modules/bootstrap/dist/css/bootstrap.min.css.map')
    .pipe(gulp.dest(`${destination}css`))
}

const moveJqueryMaps = () => {
  return gulp.src('vendors/node_modules/jquery/dist/jquery.min.map')
    .pipe(gulp.dest(`${destination}js`))
}

/* Export all functions to be able to use them in CLI
   ========================================================================== */

// Main build [Design system]
exports.cleanMain = cleanMain
exports.extractCss = extractCss
exports.extractJs = extractJs
exports.extractFonts = extractFonts
exports.moveBootstrapMap = moveBootstrapMap
exports.moveJqueryMaps = moveJqueryMaps


/* Configure the default gulp task
   ========================================================================== */

exports.default = series(cleanMain, parallel(extractCss, extractJs, extractFonts, moveBootstrapMap, moveJqueryMaps))
