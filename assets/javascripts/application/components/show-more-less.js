/* Copy to clipboard
   ========================================================================== */
$(function () {
  $('.js-copy-snippet').on('click', copySnippetToClipboard)
  $('.js-show-more-less').on('click', showMoreLess)

  $('.js-copy-snippet').mouseover(function () {
    $(this).attr('data-original-title', 'Copy to Clipboard').tooltip('fixTitle').tooltip('show')
    $(this).click(function () {
      $(this).attr('data-original-title', 'Copied!').tooltip('fixTitle').tooltip('show')
    })
    $(this).mouseout(function () {
      $(this).tooltip('hide')
    })
  })
})

function copySnippetToClipboard () {
  /* Store scss cone in vale variable  */
  const value = $(this).parent().siblings('.js-snippet-code').text()

  /* Hide tooltip after click */
  $(this).mouseout(function () {
    $("[data-toggle='tooltip']").tooltip('hide')
  })

  /* Temporarily create a wrap for the value to copy */
  const _temp = $('<textarea>')
  $('body').append(_temp)
  _temp.val(value).select()
  document.execCommand('copy')
  _temp.remove()
}

function showMoreLess () {
  /* Add toggle height class to js-snippet-code */
  $(this).siblings('.js-snippet-code').toggleClass('toggle-height')
  $(this).text('Show less')
  const checkToggleClass = $(this).siblings('.js-snippet-code').hasClass('toggle-height')

  if (checkToggleClass) {
    $(this).text('Show less')
  } else {
    $(this).text('Show more')
  }
}
