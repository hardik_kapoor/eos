/* Prepare local variables that will be reused in different methods */
let $countryTable, _monthListTr, newMonthListTr

$(function () {
  // Prepare icons containers
  $countryTable = $('.js-abbreviation-table')
  _monthListTr = $('.js-abbreviation-month-list').clone(true)

  $('.js-abbreviation-month-list').remove()

  generateMonthsAbbreviations()
})

/* Create  months abbreviations table  */
const generateMonthsAbbreviations = () => {
  /* Call getMonthsAbbreviations Service  */
  getMonthsAbbreviationsService(result => { // eslint-disable-line no-undef
    const data = result
    for (let i = 0; i < data.length; i++) {
      /* Create rows for country  */
      newMonthListTr = _monthListTr.clone(true)
      $(newMonthListTr).find('.js-abbreviation-country-name').text(data[i].name)
      const monthsName = $(newMonthListTr).find('.js-abbreviation-month-name')
      $(newMonthListTr).find('.js-abbreviation-month-name').remove()

      for (let t = 0; t < 12; t++) {
        /* Clone TD for months name  */
        const newMonthsName = monthsName.clone(true)
        $(newMonthsName).text(data[i].months[t])
        $(newMonthListTr).append(newMonthsName)
      }
      $($countryTable).append(newMonthListTr)
    }
  })
}
