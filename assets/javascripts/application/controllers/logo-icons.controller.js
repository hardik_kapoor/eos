/* ==========================================================================
  Icons version and links
  ========================================================================== */

/* Prepare local variables that will be reused in different methods */
let _logoIconsContainer, logoIconDisplayTemplate

$(function () {
  // Prepare icons containers
  _logoIconsContainer = $('.js-logo-icons-list')
  logoIconDisplayTemplate = $('.js-logo-icon-display').clone(true)
  $('.js-logo-icon-display').remove()

  // Initiate the full collection of logo icons
  if ($('.js-logoIconsController').length) {
    getLogoIconsCollection()
  }
})

const getLogoIconsCollection = () => {
  getLogoIconsService(function (logoResult) { // eslint-disable-line no-undef
    for (let i = 0; i < logoResult.length; i++) {
      const newLogoIconDisplay = logoIconDisplayTemplate.clone(true)
      // Add icon svg
      $(newLogoIconDisplay).find('.js-logo-img').attr('src', `/images/logo-icons/${logoResult[i]}`)
      // Regex to remove .svg extension
      const logoIconName = logoResult[i]
      const filenameText = logoIconName.replace(/\.[^/.]+$/, '')
      // Add icon name
      $(newLogoIconDisplay).find('.js-logo-name').text(filenameText)

      $(_logoIconsContainer).append(newLogoIconDisplay)
    }
    /* Attach the click event on creation */
    $('.js-logo-icons-set .js-panel-slidein-open').on('click', function () {
      const iconSelected = $(this).children('.js-logo-name').text()
      toggleLogoIconInPanel(iconSelected)
    })
  })
}

const toggleLogoIconInPanel = (iconName) => {
  const newlogoIconName = iconName.replace(/_/g, ' ')
  $('.js-logo-icons-name').text(newlogoIconName)
  $('.js-logo-icon-img').attr('src', `/images/logo-icons/${iconName}.svg`)
  $('.js-logo-icon-svg').attr({
    href: `/images/logo-icons/${iconName}.svg`,
    download: `${iconName}.svg`
  })
}
