const axios = require('axios')

/* Set strapi url based on env | DEVELOPMENT or PORDUCTION */
const strapiURL = process.env.NODE_ENV === 'development' || process.env.NODE_ENV === null ? process.env.EOS_STRAPI_SERVER_DEV : process.env.EOS_STRAPI_SERVER_PROD

const strapiGraphql = async query => {
  try {
    /* Auth request
      ========================================================================== */
    /* Sends a requests to the auth with username and password */
    const authRequests = await axios.post(`${strapiURL}/auth/local`, {
      identifier: process.env.EOS_STRAPI_USERNAME,
      password: process.env.EOS_STRAPI_PASSWORD
    })

    /* If credentials are correct, returns the JWT */
    const { data: { jwt } } = await authRequests

    /* Data request
      ========================================================================== */
    /* Request the data from server passing the Autorization header with the bearer token*/
    const request = await axios.get(`${strapiURL}/graphql?query={${query}}`, {
      headers: {
        Authorization: `Bearer ${jwt}`
      }
    })

    const { data } = request

    return data
  } catch (error) {
    console.log('ERROR => strapiGraphql(): ', error);
  }
}

module.exports = {
  strapiGraphql
}
